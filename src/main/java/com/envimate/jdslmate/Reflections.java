/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.jdslmate;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import static java.util.Arrays.stream;

final class Reflections {

    private Reflections() {
    }

    static <T> T instantiateWithZeroArgumentConstructor(final Class<T> type) {
        try {
            final Constructor<?> constructor = type.getConstructor();
            final Object object = constructor.newInstance();
            return (T) object;
        } catch (final NoSuchMethodException e) {
            throw new UnsupportedOperationException("Zero arguments constructor required.", e);
        } catch (final IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new RuntimeException(e);
        }
    }

    static <T> T instantiateWithDSLMateArgumentConstructor(final Class<T> type, final JDSLMate JDSLMate) {
        try {
            final Constructor<?> constructor = type.getConstructor(JDSLMate.class);
            final Object object = constructor.newInstance(JDSLMate);
            return (T) object;
        } catch (final NoSuchMethodException e) {
            throw new UnsupportedOperationException("Zero arguments constructor required.", e);
        } catch (final IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new RuntimeException(e);
        }
    }

    static <T> T instantiateWithZeroArgumentConstructorAndStateAndDSLMate(final Class<T> type,
                                                                          final Object state,
                                                                          final JDSLMate JDSLMate) {
        final T instance = instantiateWithZeroArgumentConstructor(type);
        final Field stateField = findStateField(type);
        final Field dslMateField = findDSLMateField(type);
        setField(stateField, instance, state);
        setField(dslMateField, instance, JDSLMate);
        return instance;
    }

    static Class<?> getStateClass(final Class<?> builderClass) {
        final Field stateField = findStateField(builderClass);
        return stateField.getType();
    }

    private static Field findStateField(final Class<?> builderClass) {
        final Field[] fields = builderClass.getDeclaredFields();
        if (fields.length != 2) {
            throw new IllegalArgumentException("Builder class does not declare two fields: " + builderClass.getSimpleName());
        }
        final Field stateField = fieldThatIsNotOfTypeDSLMate(fields);
        return stateField;
    }

    private static Field fieldThatIsNotOfTypeDSLMate(final Field[] fields) {
        return stream(fields)
                .filter(field -> !field.getType().isAssignableFrom(JDSLMate.class))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    private static Field findDSLMateField(final Class<?> builderClass) {
        final Field[] fields = builderClass.getDeclaredFields();
        return stream(fields)
                .filter(field -> field.getType().isAssignableFrom(JDSLMate.class))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    private static void setField(final Field field,
                                 final Object instance,
                                 final Object value) {
        field.setAccessible(true);
        try {
            field.set(instance, value);
        } catch (final IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}

