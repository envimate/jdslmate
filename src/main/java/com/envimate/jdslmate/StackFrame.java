/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.jdslmate;

public final class StackFrame {

    private final ReturnAction returnAction;
    private Object state;

    private StackFrame(final ReturnAction<?> returnAction) {
        this.returnAction = returnAction;
        this.state = null;
    }

    static StackFrame initStackFrame(final ReturnAction<?> returnAction) {
        return new StackFrame(returnAction);
    }

    @SuppressWarnings("unchecked")
    <T> T getState() {
        return (T) this.state;
    }

    void setState(final Object state) {
        this.state = state;
    }

    @SuppressWarnings("unchecked")
    <T> T doReturn(final Object returnValue) {
        return (T) this.returnAction.doReturn(returnValue);
    }
}
