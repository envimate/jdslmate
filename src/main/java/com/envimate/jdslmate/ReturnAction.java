/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.jdslmate;

@FunctionalInterface
public interface ReturnAction<T> {
    Object doReturn(T returnValue);
}
