/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.jdslmate;

import java.util.Stack;
import java.util.function.Supplier;

import static com.envimate.jdslmate.Reflections.instantiateWithDSLMateArgumentConstructor;
import static com.envimate.jdslmate.StackFrame.initStackFrame;

public final class JDSLMate {

    private final Stack<StackFrame> stack;

    private JDSLMate() {
        this.stack = new Stack<>();
    }

    public static <T> T enterDSL(final Class<T> entryClass) {
        final JDSLMate JDSLMate = new JDSLMate();
        return JDSLMate.call(entryClass, (returnValue) -> {
            throw new UnsupportedOperationException();
        });
    }

    public <T> T getStateOrInitVia(final Supplier<T> constructor) {
        final StackFrame currentFrame = this.stack.peek();
        final T queriedState = currentFrame.getState();
        if(queriedState != null) {
            return queriedState;
        }
        final T createdState = constructor.get();
        currentFrame.setState(createdState);
        return createdState;
    }

    public <T> T call(final Class<T> function,
                      final ReturnAction<?> returnAction) {
        final StackFrame stackFrame = initStackFrame(returnAction);
        this.stack.push(stackFrame);
        return goTo(function);
    }

    public <T> T reTurn(final Object returnValue) {
        final StackFrame stackFrame = this.stack.pop();
        return stackFrame.doReturn(returnValue);
    }

    public <T> T goTo(final Class<T> target) {
        return instantiateWithDSLMateArgumentConstructor(target, this);
    }
}
